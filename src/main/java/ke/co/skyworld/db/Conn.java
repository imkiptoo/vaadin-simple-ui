package ke.co.skyworld.db;

import com.vaadin.shared.Position;
import com.vaadin.ui.Notification;
import ke.co.skyworld.ui.event.Notifications;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*
 * Created by elon on 3/8/2017.
 */
public class Conn {
    public Conn(){}

    public static DataSource dataSource1()
    {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.url("jdbc:mysql://localhost/cedar_kejapay?autoReconnect=true&useSSL=false");
        dataSourceBuilder.username("root");
        dataSourceBuilder.password("");
        dataSourceBuilder.driverClassName("com.mysql.jdbc.Driver");
        return dataSourceBuilder.build();
    }

    public static Connection getConn() throws SQLException {
         Connection conn = null;

         try{

             conn = DriverManager.getConnection(
                     "jdbc:mysql://localhost:3306/cedar_kejapay?autoReconnect=true&useSSL=false",
                     "root",
                     "");

             return conn;
         }catch(Exception e){
             Notifications.showNotification("Error!", "<span>Unable to Connect to our services. Kindly retry later.</span>", Notification.Type.ERROR_MESSAGE, 3000, "", Position.TOP_CENTER);
         }

        return null;
    }
}
