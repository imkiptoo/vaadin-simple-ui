package ke.co.skyworld.ui.views;

import com.vaadin.event.LayoutEvents;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Responsive;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import ke.co.skyworld.ui.event.DashboardEvent;
import ke.co.skyworld.ui.event.DashboardEventBus;

/*
 * Created by elon on 3/7/2017.
 */
public class Bills extends Panel implements View{

    private Label titleLabel;
    private final VerticalLayout root;

    public Bills(){
        addStyleName(ValoTheme.PANEL_BORDERLESS);
        setSizeFull();
        DashboardEventBus.register(this);

        root = new VerticalLayout();
        root.setSizeFull();
        root.setMargin(true);
        root.addStyleName("dashboard-view");
        setContent(root);
        Responsive.makeResponsive(root);

        root.addComponent(buildHeader());

        //root.addComponent(buildDashboardStats());

        //Component content = buildContent();
        //root.addComponent(content);
        //root.setExpandRatio(content, 1);

        // All the open sub-windows should be closed whenever the root layout
        // gets clicked.
        root.addLayoutClickListener(new LayoutEvents.LayoutClickListener() {
            @Override
            public void layoutClick(final LayoutEvents.LayoutClickEvent event) {
                DashboardEventBus.post(new DashboardEvent.CloseOpenWindowsEvent());
            }
        });
    }

    private Component buildHeader() {
        VerticalLayout verticalLayoutHeader = new VerticalLayout();
        HorizontalLayout header = new HorizontalLayout();
        HorizontalLayout subHeader = new HorizontalLayout();
        header.setWidth("100%");
        header.setSpacing(true);

        subHeader.setWidth("100%");
        subHeader.setSpacing(true);

        titleLabel = new Label("Bills");
        titleLabel.setSizeUndefined();
        titleLabel.addStyleName(ValoTheme.LABEL_H1);
        titleLabel.addStyleName(ValoTheme.LABEL_NO_MARGIN);
        header.addComponent(titleLabel);

        titleLabel = new Label("KejaPay / Bills");
        titleLabel.setSizeUndefined();
        titleLabel.addStyleName(ValoTheme.LABEL_H3);
        subHeader.addComponent(titleLabel);

        verticalLayoutHeader.addComponents(header, subHeader);
        verticalLayoutHeader.setComponentAlignment(subHeader, Alignment.BOTTOM_LEFT);

        return verticalLayoutHeader;
    }




    @Override
    public void enter(final ViewChangeListener.ViewChangeEvent event) {

    }
}