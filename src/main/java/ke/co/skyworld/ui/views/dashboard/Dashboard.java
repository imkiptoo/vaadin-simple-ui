package ke.co.skyworld.ui.views.dashboard;

import com.vaadin.event.LayoutEvents;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Responsive;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import ke.co.skyworld.ui.component.SparklineChart;
import ke.co.skyworld.ui.event.DashboardEvent;
import ke.co.skyworld.ui.event.DashboardEventBus;

import java.util.Iterator;

/*
 * Created by elon on 3/7/2017.
 */
public class Dashboard extends Panel implements View {

    private Label titleLabel;
    private final VerticalLayout root;
    private CssLayout dashboardPanels;

    public Dashboard() {
        addStyleName(ValoTheme.PANEL_BORDERLESS);
        setSizeFull();
        DashboardEventBus.register(this);

        root = new VerticalLayout();
        root.setSizeFull();
        root.setMargin(true);
        root.addStyleName("dashboard-view");
        setContent(root);
        Responsive.makeResponsive(root);

        root.addComponent(buildHeader());

        Component content = buildContent();
        root.addComponent(content);
        root.setExpandRatio(content, 1);

        // All the open sub-windows should be closed whenever the root layout
        // gets clicked.
        root.addLayoutClickListener(new LayoutEvents.LayoutClickListener() {
            @Override
            public void layoutClick(final LayoutEvents.LayoutClickEvent event) {
                DashboardEventBus.post(new DashboardEvent.CloseOpenWindowsEvent());
            }
        });
    }

    private Component buildDashboardStats() {
        CssLayout dashboardStats = new CssLayout();
        dashboardStats.addStyleName("sparks");
        dashboardStats.setWidth("100%");
        Responsive.makeResponsive(dashboardStats);

        /*HorizontalLayout horizontalLayoutTotalRevenue = new HorizontalLayout(buildTotalRevenue());

        dashboardStats.addComponent(horizontalLayoutTotalRevenue);*/

        SparklineChart s = new SparklineChart("Total Revenue", "M", "$", "1.24");
        dashboardStats.addComponent(s);
        s.setSpacing(false);
        s.setMargin(false);

        s = new SparklineChart("Landlords", "", "", "10");
        dashboardStats.addComponent(s);

        s = new SparklineChart("Properties", "", "", "50");
        dashboardStats.addComponent(s);

        s = new SparklineChart("Tenats", "", "", "1520");
        dashboardStats.addComponent(s);

        dashboardStats.addComponent(s);

        return dashboardStats;
    }


    private Component buildHeader() {
        VerticalLayout verticalLayoutHeader = new VerticalLayout();
        HorizontalLayout header = new HorizontalLayout();
        HorizontalLayout subHeader = new HorizontalLayout();
        //header.addStyleName("viewheader");
        header.setWidth("100%");
        header.setSpacing(true);

        //subHeader.addStyleName(ValoTheme.);
        subHeader.setWidth("100%");
        subHeader.setSpacing(true);

        titleLabel = new Label("Dashboard");
        titleLabel.setSizeUndefined();
        titleLabel.addStyleName(ValoTheme.LABEL_H1);
        titleLabel.addStyleName(ValoTheme.LABEL_NO_MARGIN);
        header.addComponent(titleLabel);

        titleLabel = new Label("Welcome to KejaPay admin panel");
        titleLabel.setSizeUndefined();
        titleLabel.addStyleName(ValoTheme.LABEL_H3);
        //titleLabel.addStyleName(ValoTheme.LABEL_NO_MARGIN);
        subHeader.addComponent(titleLabel);

        verticalLayoutHeader.addComponents(header, subHeader);
        verticalLayoutHeader.setComponentAlignment(subHeader, Alignment.BOTTOM_LEFT);

        return verticalLayoutHeader;
    }

    private Component buildContent() {
        dashboardPanels = new CssLayout();
        dashboardPanels.addStyleName("dashboard-panels");
        Responsive.makeResponsive(dashboardPanels);

        //dashboardPanels.addComponent(buildTopGrossingMovies());
        dashboardPanels.addComponent(buildDashboardStats());
        //dashboardPanels.addComponent(buildTop10TitlesByRevenue());
        //dashboardPanels.addComponent(buildPopularMovies());

        return dashboardPanels;
    }

    private Component buildNotes() {
        TextArea notes = new TextArea("Notes");
        notes.setValue("Remember to:\n· Zoom in and out in the Sales view\n· Filter the transactions and drag a set of them to the Reports tab\n· Create a new report\n· Change the schedule of the movie theater");
        notes.setSizeFull();
        notes.addStyleName(ValoTheme.TEXTAREA_BORDERLESS);
        Component panel = createContentWrapper(notes);
        panel.addStyleName("notes");
        return panel;
    }

    private Component createContentWrapper(final Component content) {
        final CssLayout slot = new CssLayout();
        slot.setWidth("100%");
        slot.addStyleName("dashboard-panel-slot");

        CssLayout card = new CssLayout();
        card.setWidth("100%");
        card.addStyleName(ValoTheme.LAYOUT_CARD);

        HorizontalLayout toolbar = new HorizontalLayout();
        toolbar.addStyleName("dashboard-panel-toolbar");
        toolbar.setWidth("100%");

        Label caption = new Label(content.getCaption());
        caption.addStyleName(ValoTheme.LABEL_H4);
        caption.addStyleName(ValoTheme.LABEL_COLORED);
        caption.addStyleName(ValoTheme.LABEL_NO_MARGIN);
        content.setCaption(null);

        MenuBar tools = new MenuBar();
        tools.addStyleName(ValoTheme.MENUBAR_BORDERLESS);
        MenuBar.MenuItem max = tools.addItem("", FontAwesome.EXPAND, new MenuBar.Command() {

            @Override
            public void menuSelected(final MenuBar.MenuItem selectedItem) {
                if (!slot.getStyleName().contains("max")) {
                    selectedItem.setIcon(FontAwesome.COMPRESS);
                    toggleMaximized(slot, true);
                } else {
                    slot.removeStyleName("max");
                    selectedItem.setIcon(FontAwesome.EXPAND);
                    toggleMaximized(slot, false);
                }
            }
        });
        max.setStyleName("icon-only");

        toolbar.addComponents(caption, tools);
        toolbar.setExpandRatio(caption, 1);
        toolbar.setComponentAlignment(caption, Alignment.MIDDLE_LEFT);

        card.addComponents(toolbar, content);
        slot.addComponent(card);
        return slot;
    }

    private void toggleMaximized(final Component panel, final boolean maximized) {
        for (Iterator<Component> it = root.iterator(); it.hasNext();) {
            it.next().setVisible(!maximized);
        }
        dashboardPanels.setVisible(true);

        for (Iterator<Component> it = dashboardPanels.iterator(); it.hasNext();) {
            Component c = it.next();
            c.setVisible(!maximized);
        }

        if (maximized) {
            panel.setVisible(true);
            panel.addStyleName("max");
        } else {
            panel.removeStyleName("max");
        }
    }


    @Override
    public void enter(final ViewChangeListener.ViewChangeEvent event) {

    }
}
