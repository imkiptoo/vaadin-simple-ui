package ke.co.skyworld.ui.data.data_from_db;

import com.vaadin.shared.Position;
import com.vaadin.ui.Notification;
import ke.co.skyworld.db.Conn;
import ke.co.skyworld.ui.domain.*;
import ke.co.skyworld.ui.event.Notifications;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Collection;
import java.util.Date;

/*
 * Created by elon on 3/8/2017.
 */
public class DataProvider implements ke.co.skyworld.ui.data.DataProvider {

    @Override
    public User authenticate(String userName, String password) {
        try{

            Connection conn = Conn.getConn();

            Statement sqlQueryStatement = conn.createStatement();

            String sqlQuery = "SELECT * FROM user_accounts WHERE password = '"+password+"' AND (username = '"+userName+"' OR email_address = '"+userName+"')";

            ResultSet rs = sqlQueryStatement.executeQuery(sqlQuery);

            if(!rs.next()) {
                Notifications.showNotification("Error!", "<span>Unable to log in. This may be to incorrect credentials.</span>", Notification.Type.ERROR_MESSAGE, 3000, "", Position.TOP_CENTER);
                return null;
            }

            System.out.println("Executed Query!");

            User user = new User(
                    Integer.toString(rs.getInt("user_id")),
                    rs.getString("username"),
                    rs.getString("email_address"),
                    rs.getString("phone_number"),
                    rs.getString("password"),
                    rs.getString("first_name"),
                    rs.getString("last_name"),
                    rs.getString("gender"),
                    rs.getString("national_id_number"),
                    rs.getString("status"),
                    rs.getString("date_added"),
                    rs.getString("designation"),
                    rs.getString("authorisation_level"),
                    rs.getString("bio")
            );

            conn.close();

            return user;



        }catch(Exception e){
            System.out.print("Error in Executing Query! : "+e.toString());
        }

        return null;
    }

    @Override
    public void updateUser(User user){
        try{
            Connection conn = Conn.getConn();

            Statement sqlQueryStatement = conn.createStatement();

            String sqlQuery = "UPDATE `cedar_kejapay`.`user_accounts` SET " +
                    "`username`='"+user.getUserName()+"',"+
                    "`email_address`='"+user.getEmail()+"'," +
                    "`phone_number`="+user.getPhoneNumben()+"," +
                    "`password`="+user.getPassword()+"," +
                    "`first_name`='"+user.getFirstName()+"'," +
                    "`last_name`='"+user.getLastName()+"', " +
                    "`gender`='"+user.getGender()+"', " +
                    "`national_id_number`="+user.getNationalID()+", " +
                    "`bio`='"+user.getBio()+"'" +
                    " WHERE `user_id`="+user.getUserId();

            System.out.println("UPDATE `cedar_kejapay`.`user_accounts` SET " +
                    "`email_address`='"+user.getEmail()+"'," +
                    "`phone_number`="+user.getPhoneNumben()+"," +
                    "`password`="+user.getPassword()+"," +
                    "`first_name`='"+user.getFirstName()+"'," +
                    "`last_name`='"+user.getLastName()+"', " +
                    "`national_id_number`="+user.getNationalID()+", " +
                    "`bio`='"+user.getBio()+"'" +
                    " WHERE `username`='"+user.getUserId()+"'");

           sqlQueryStatement.execute(sqlQuery);

            System.out.println("Executed Query2!");

            conn.close();
        }catch(Exception e){
            System.out.print("Error in Executing Query! : "+e.toString());
        }
    }
}
