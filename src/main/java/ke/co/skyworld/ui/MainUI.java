package ke.co.skyworld.ui;

import com.google.common.eventbus.Subscribe;
import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.server.Page;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.server.Page.BrowserWindowResizeListener;
import com.vaadin.ui.themes.ValoTheme;
import ke.co.skyworld.ui.data.DataProvider;
import ke.co.skyworld.ui.domain.User;
import ke.co.skyworld.ui.event.DashboardEvent;
import ke.co.skyworld.ui.event.DashboardEventBus;
import ke.co.skyworld.ui.event.Notifications;
import ke.co.skyworld.ui.views.Login;
import ke.co.skyworld.ui.views.dashboard.MainView;

import java.util.Locale;

/**
 * Created by elon on 3/3/2017 - 11:15 PM.
 * Package: ke.co.skyworld.frontend.ui.
 * Project: vaadinUISample.
 */

@SpringUI
/*@Theme("valo")*/
@Theme("dashboard")
@Title("KejaPay Admin Panel")
/*@Widgetset("ke.co.skyworld.dashboard.DashboardWidgetSet")*/
@SuppressWarnings("serial")
@PreserveOnRefresh
public class MainUI extends UI {

    private final DataProvider dataProvider = new ke.co.skyworld.ui.data.data_from_db.DataProvider();
    private final DashboardEventBus dashboardEventbus = new DashboardEventBus();

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setLocale(Locale.getDefault());

        DashboardEventBus.register(this);
        Responsive.makeResponsive(this);
        addStyleName(ValoTheme.UI_WITH_MENU);

        /*setContent(new Login());
        addStyleName("loginview");*/

        updateContent();

        // Some views need to be aware of browser resize events so a
        // BrowserResizeEvent gets fired to the event bus on every occasion.
        Page.getCurrent().addBrowserWindowResizeListener(
                new BrowserWindowResizeListener() {
                    @Override
                    public void browserWindowResized(
                            final Page.BrowserWindowResizeEvent event) {
                        DashboardEventBus.post(new DashboardEvent.BrowserResizeEvent());
                    }
                });

        /*Navigator navigator = new Navigator(this, this);

        //Login
        navigator.addView("login", new Login());

        //dashboard
        navigator.addView("dashboard", new Dashboard());

        //_init_ ui
        navigator.navigateTo("login");*/
    }

    /**
     * Updates the correct content for this UI based on the current user status.
     * If the user is logged in with appropriate privileges, main view is shown.
     * Otherwise login view is shown.
     */
    private void updateContent() {
        User user = (User) VaadinSession.getCurrent().getAttribute(
                User.class.getName());
        if (user != null) {
            // Authenticated user
            setContent(new MainView());
            removeStyleName("loginview");
            getNavigator().navigateTo(getNavigator().getState());
        } else {
            setContent(new Login());
            addStyleName("loginview");
        }
    }

    @Subscribe
    public void updateUser(final  DashboardEvent.UpdateUserEvent event){
        getDataProvider().updateUser(event.getUserDetails());
    }

    @Subscribe
    public void userLoginRequested(final DashboardEvent.UserLoginRequestedEvent event) {
        User user = getDataProvider().authenticate(event.getPhoneNumber(),
                event.getPin());
        VaadinSession.getCurrent().setAttribute(User.class.getName(), user);
        updateContent();
    }

    @Subscribe
    public void userLoggedOut(final DashboardEvent.UserLoggedOutEvent event) {
        // When the user logs out, current VaadinSession gets closed and the
        // page gets reloaded on the login screen. Do notice the this doesn't
        // invalidate the current HttpSession.
        VaadinSession.getCurrent().close();
        Page.getCurrent().reload();
    }

    @Subscribe
    public void closeOpenWindows(final DashboardEvent.CloseOpenWindowsEvent event) {
        for (Window window : getWindows()) {
            window.close();
        }
    }

    /**
     * @return An instance for accessing the data services layer.
     */

    public static DashboardEventBus getDashboardEventbus() {
        return ((MainUI) getCurrent()).dashboardEventbus;
    }

    public static DataProvider getDataProvider() {
        return ((MainUI) getCurrent()).dataProvider;
    }

    @Override
    protected void refresh(VaadinRequest request) {
        Notifications.showNotification("Refresh.", "UI was refreshed @"
                + System.currentTimeMillis(), Notification.Type.ASSISTIVE_NOTIFICATION, 3000,"tray dark small closable login-help", Position.TOP_CENTER);
    }
}
