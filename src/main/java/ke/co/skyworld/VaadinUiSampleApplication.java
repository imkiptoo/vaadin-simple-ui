package ke.co.skyworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VaadinUiSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(VaadinUiSampleApplication.class, args);
	}
}
